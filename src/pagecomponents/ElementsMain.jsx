import React from "react";
import ElementsButtons from "./ElementsButtons";
import ElementsText from "./ElementsText";

const ElementsMain = () => {
  return (
    <div id="main" className="container">
      <h2 id="elements">Elements</h2>
      <div className="row 200%">
        <ElementsText/>
        <ElementsButtons/>
      </div>
    </div>
  );
};

export default ElementsMain;
