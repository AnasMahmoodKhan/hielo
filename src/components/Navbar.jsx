import React from 'react'

const Navbar = () => {
    return (
        <nav id="menu">
				<ul className="links">
					<li><a href="/">Home</a></li>
					<li><a href="/generic">Generic</a></li>
					<li><a href="/elements">Elements</a></li>
				</ul>
			</nav>
    )
}

export default Navbar
