import React from 'react'
import GenericOne from '../pagecomponents/GenericOne'
import GenericTwo from '../pagecomponents/GenericTwo'

const Generic = () => {
    return (
        <React.Fragment>
            <GenericOne/>
            <GenericTwo/>
        </React.Fragment>
    )
}

export default Generic
