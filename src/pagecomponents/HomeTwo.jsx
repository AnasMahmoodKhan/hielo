import React from "react";

const HomeTwo = () => {
  return (
    <section id="two" className="wrapper style3">
      <div className="inner">
        <header className="align-center">
          <p>
            Nam vel ante sit amet libero scelerisque facilisis eleifend vitae
            urna
          </p>
          <h2>Morbi maximus justo</h2>
        </header>
      </div>
    </section>
  );
};

export default HomeTwo;
