import React from "react";

const ElementsOne = () => {
  return (
    <section id="One" className="wrapper style3">
      <div className="inner">
        <header className="align-center">
          <p>Sed amet nulla</p>
          <h2>Elements</h2>
        </header>
      </div>
    </section>
  );
};

export default ElementsOne;
