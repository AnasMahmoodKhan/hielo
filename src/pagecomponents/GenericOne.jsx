import React from 'react'

const GenericOne = () => {
    return (
        <section id="One" className="wrapper style3">
				<div className="inner">
					<header className="align-center">
						<p>Eleifend vitae urna</p>
						<h2>Generic Page Template</h2>
					</header>
				</div>
			</section>
    )
}

export default GenericOne
