import React from "react";
import Banner from "../components/Banner";
import HomeOne from "../pagecomponents/HomeOne";
import HomeThree from "../pagecomponents/HomeThree";
import HomeTwo from "../pagecomponents/HomeTwo";

const Home = () => {
  return (
    <React.Fragment>
      <Banner />
      <HomeOne />
      <HomeTwo />
      <HomeThree />
    </React.Fragment>
  );
};

export default Home;
