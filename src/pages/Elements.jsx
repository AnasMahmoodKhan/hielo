import React from 'react'
import ElementsMain from '../pagecomponents/ElementsMain'
import ElementsOne from '../pagecomponents/ElementsOne'

const Elements = () => {
    return (
        <React.Fragment>
            <ElementsOne/>
            <ElementsMain/>
        </React.Fragment>
    )
}

export default Elements
