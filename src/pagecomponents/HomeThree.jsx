import React from "react";

const HomeThree = () => {
  return (
    <section id="three" className="wrapper style2">
      <div className="inner">
        <header className="align-center">
          <p className="special">
            Nam vel ante sit amet libero scelerisque facilisis eleifend vitae
            urna
          </p>
          <h2>Morbi maximus justo</h2>
        </header>
        <div className="gallery">
          <div>
            <div className="image fit">
              <a href="#">
                <img src="images/pic01.jpg" alt="" />
              </a>
            </div>
          </div>
          <div>
            <div className="image fit">
              <a href="#">
                <img src="images/pic02.jpg" alt="" />
              </a>
            </div>
          </div>
          <div>
            <div className="image fit">
              <a href="#">
                <img src="images/pic03.jpg" alt="" />
              </a>
            </div>
          </div>
          <div>
            <div className="image fit">
              <a href="#">
                <img src="images/pic04.jpg" alt="" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HomeThree;
