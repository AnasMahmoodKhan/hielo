import React from 'react'

const ElementsButtons = () => {
    return (
        <div className="6u$ 12u$(medium)">
          <h3>Buttons</h3>
          <ul className="actions">
            <li>
              <a href="#" className="button special">
                Special
              </a>
            </li>
            <li>
              <a href="#" className="button">
                Default
              </a>
            </li>
            <li>
              <a href="#" className="button alt">
                Alternate
              </a>
            </li>
          </ul>
          <ul className="actions">
            <li>
              <a href="#" className="button special big">
                Big
              </a>
            </li>
            <li>
              <a href="#" className="button">
                Default
              </a>
            </li>
            <li>
              <a href="#" className="button alt small">
                Small
              </a>
            </li>
          </ul>
          <ul className="actions fit">
            <li>
              <a href="#" className="button special fit">
                Fit
              </a>
            </li>
            <li>
              <a href="#" className="button fit">
                Fit
              </a>
            </li>
            <li>
              <a href="#" className="button alt fit">
                Fit
              </a>
            </li>
          </ul>
          <ul className="actions fit small">
            <li>
              <a href="#" className="button special fit small">
                Fit + Small
              </a>
            </li>
            <li>
              <a href="#" className="button fit small">
                Fit + Small
              </a>
            </li>
            <li>
              <a href="#" className="button alt fit small">
                Fit + Small
              </a>
            </li>
          </ul>
          <ul className="actions">
            <li>
              <a href="#" className="button special icon fa-search">
                Icon
              </a>
            </li>
            <li>
              <a href="#" className="button icon fa-download">
                Icon
              </a>
            </li>
            <li>
              <a href="#" className="button alt icon fa-check">
                Icon
              </a>
            </li>
          </ul>
          <ul className="actions">
            <li>
              <span className="button special disabled">Special</span>
            </li>
            <li>
              <span className="button disabled">Default</span>
            </li>
            <li>
              <span className="button alt disabled">Alternate</span>
            </li>
          </ul>

          <h3>Form</h3>

          <form method="post" action="#">
            <div className="row uniform">
              <div className="6u 12u$(xsmall)">
                <input
                  type="text"
                  name="name"
                  id="name"
                  value=""
                  placeholder="Name"
                />
              </div>
              <div className="6u$ 12u$(xsmall)">
                <input
                  type="email"
                  name="email"
                  id="email"
                  value=""
                  placeholder="Email"
                />
              </div>

              <div className="12u$">
                <div className="select-wrapper">
                  <select name="category" id="category">
                    <option value="">- Category -</option>
                    <option value="1">Manufacturing</option>
                    <option value="1">Shipping</option>
                    <option value="1">Administration</option>
                    <option value="1">Human Resources</option>
                  </select>
                </div>
              </div>

              <div className="4u 12u$(small)">
                <input type="radio" id="priority-low" name="priority" checked />
                <label for="priority-low">Low Priority</label>
              </div>
              <div className="4u 12u$(small)">
                <input type="radio" id="priority-normal" name="priority" />
                <label for="priority-normal">Normal Priority</label>
              </div>
              <div className="4u$ 12u$(small)">
                <input type="radio" id="priority-high" name="priority" />
                <label for="priority-high">High Priority</label>
              </div>

              <div className="6u 12u$(small)">
                <input type="checkbox" id="copy" name="copy" />
                <label for="copy">Email me a copy of this message</label>
              </div>
              <div className="6u$ 12u$(small)">
                <input type="checkbox" id="human" name="human" checked />
                <label for="human">I am a human and not a robot</label>
              </div>

              <div className="12u$">
                <textarea
                  name="message"
                  id="message"
                  placeholder="Enter your message"
                  rows="6"
                ></textarea>
              </div>

              <div className="12u$">
                <ul className="actions">
                  <li>
                    <input type="submit" value="Send Message" />
                  </li>
                  <li>
                    <input type="reset" value="Reset" className="alt" />
                  </li>
                </ul>
              </div>
            </div>
          </form>

          <hr />

          <form method="post" action="#">
            <div className="row uniform">
              <div className="9u 12u$(small)">
                <input
                  type="text"
                  name="query"
                  id="query"
                  value=""
                  placeholder="Query"
                />
              </div>
              <div className="3u$ 12u$(small)">
                <input type="submit" value="Search" className="fit" />
              </div>
            </div>
          </form>

          <h3>Image</h3>

          <h4>Fit</h4>
          <span className="image fit">
            <img src="images/pic01.jpg" alt="" />
          </span>
          <div className="box alt">
            <div className="row 50% uniform">
              <div className="4u">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>
              <div className="4u">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>
              <div className="4u$">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>

              <div className="4u">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>
              <div className="4u">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>
              <div className="4u$">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>

              <div className="4u">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>
              <div className="4u">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>
              <div className="4u$">
                <span className="image fit">
                  <img src="images/pic01.jpg" alt="" />
                </span>
              </div>
            </div>
          </div>

          <h4>Left &amp; Right</h4>
          <p>
            <span className="image left">
              <img src="images/pic02.jpg" alt="" />
            </span>
            Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis
            sagittis eget. tempus euismod. Vestibulum ante ipsum primis in
            faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac
            adipiscing accumsan eu faucibus. Integer ac pellentesque praesent
            tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum
            primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis
            in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat
            ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent
            tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum
            primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis
            volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque
            praesent. Vestibulum ante ipsum primis in faucibus magna blandit
            adipiscing eu felis iaculis volutpat lorem ipsum dolor sit amet
            dolor consequat.
          </p>
          <p>
            <span className="image right">
              <img src="images/pic02.jpg" alt="" />
            </span>
            Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis
            sagittis eget. tempus euismod. Vestibulum ante ipsum primis in
            faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac
            adipiscing accumsan eu faucibus. Integer ac pellentesque praesent
            tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum
            primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis
            in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat
            ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent
            tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum
            primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis
            volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque
            praesent. Vestibulum ante ipsum primis in faucibus magna blandit
            adipiscing eu felis iaculis volutpat lorem ipsum dolor sit amet
            dolor consequat.
          </p>

          <h3>Box</h3>
          <div className="box">
            <p>
              Felis sagittis eget tempus primis in faucibus vestibulum. Blandit
              adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu
              faucibus. Integer ac pellentesque praesent tincidunt felis
              sagittis eget. tempus euismod. Magna sed etiam ante ipsum primis
              in faucibus vestibulum. Blandit adipiscing eu ipsum primis in
              faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat
              ac adipiscing accumsan eu faucibus lorem ipsum dolor sit amet
              nullam.
            </p>
          </div>

          <h3>Preformatted</h3>
        </div>
    )
}

export default ElementsButtons
